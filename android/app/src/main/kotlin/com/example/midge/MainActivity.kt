package com.example.midge

import android.os.Environment;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

import java.io.File;

class MainActivity: FlutterActivity() {

    private val CHANNEL = "midge.flutter.dev/music"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            // Note: this method is invoked on the main thread.
            call, result ->
            if (call.method == "getExternalStorageDirectory") {
              val dir = getExternalStorageDirectory()
      
              if (dir != null) {
                result.success(dir)
              } else {
                result.error("UNAVAILABLE", "Couldn't get directory.", null)
              }
            } else if (call.method == "getMp3FolderPaths") {
              val folders = getMp3FolderPaths();
      
              if (folders != null) {
                result.success(folders)
              } else {
                result.error("UNAVAILABLE", "Couldn't get folders.", null)
              }
            } else if (call.method == "getMp3FilesFromFolderPath") {

              val folderPath: String? = call.argument("folderPath");
              print("folderPath : >> " + folderPath)
              val folderFiles = getMp3FilesFromFolderPath(folderPath.toString());

              if (folderFiles != null) {
                result.success(folderFiles)
              } else {
                result.error("UNAVAILABLE", "Couldn't get files.", null)
              }
            } else {
              result.notImplemented()
            }
          }
    }

    private fun getExternalStorageDirectory(): String {
    return Environment.getExternalStorageDirectory().toString();
  }

  private fun getMp3FolderPaths() : List<Map<String, String>> { 

    val toReturn = getMp3FolderPaths(Environment.getExternalStorageDirectory().toString())

    // val result = capitals.toList().sortedBy { (_, value) -> value}.toMap()
    // if (toReturn != null) {
    //  val result = toReturn.toList().sortedBy { (_, value) -> value.mp3FileCount }.toMap();
    // }
    return toReturn;
  }

  private fun getMp3FolderPaths(rootPath : String) : List<Map<String, String>> {

    val folderList = ArrayList<Map<String, String>>();
      val rootFolder = File(rootPath);
      val files = rootFolder.listFiles();
      var mp3FileCount: Int = 0;
      for (file in files) {
          if (file.isDirectory()) {
            folderList.addAll(getMp3FolderPaths(file.getAbsolutePath())); 
          } else if (file.getName().endsWith(".mp3")) {
            mp3FileCount++;
          }
      }

      if (mp3FileCount > 0) {
        val foldermap = HashMap<String, String>();
        foldermap.put("file_path", rootFolder.getAbsolutePath());
        foldermap.put("file_name", rootFolder.getName());
        foldermap.put("mp3_Count", mp3FileCount.toString());
        folderList.add(foldermap);
      }
      
      return folderList;
  }

  private fun getMp3FilesFromFolderPath(rootPath : String) : List<Map<String, String>> {

    var rootFolder = File(rootPath);
    if (rootPath == null) {
      var str = "/storage/emulated/0/Music/Hindi Songs/3 Idiots";
      rootFolder = File(str.toString());
    }
    
      val files = rootFolder.listFiles();
      val fileList = ArrayList<Map<String, String>>();

      for (file in files) {
        if (file.getName().endsWith(".mp3")) {
          val fileMap = HashMap<String, String>();
          fileMap.put("file_path", file.getAbsolutePath());
          fileMap.put("file_name", file.getName());
          fileMap.put("file_modified", file.lastModified().toString());
          fileList.add(fileMap);
        }
    }
    return fileList;
  }
}
