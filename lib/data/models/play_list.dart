import 'package:flutter/widgets.dart';

class PlayList extends ChangeNotifier {
  /// Internal, private state of the cart.
  final List<Map<String, String>> _items = [];

  final List<String> selectedFolders = [];

  /// An unmodifiable view of the items in the cart.
  //UnmodifiableListView<Item> get items => UnmodifiableListView(_items);

  /// The current total price of all items (assuming all items cost $42).
  int get totalPrice => _items.length * 42;

  /// Adds [item] to cart. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(Map<String, String> item) {
    _items.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  List<Map<String, String>> getItems() {
    return _items;
  }

  void addAll(List<Map<String, String>> items) {
    _items.addAll(items);
    notifyListeners();
  }

  /// Removes all items from the cart.
  void removeAll() {
    _items.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  List<String> getSelectedFolders() {
    return this.selectedFolders;
  }

  void addSelectedFolders(String name) {
    this.selectedFolders.add(name);
    notifyListeners();
  }

  void removeSelectedFolders() {
    selectedFolders.clear();
    notifyListeners();
  }
}
