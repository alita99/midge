import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:midge/data/models/model.dart';

abstract class DB {
  // static Database _db;

  // static int get _version => 1;

  // static Future<void> init() async {
  //   if (_db != null) {
  //     return;
  //   }

  //   try {
  //     String _path = await getDatabasesPath() + 'example';
  //     _db = await openDatabase(_path, version: _version, onCreate: onCreate);
  //   } catch (ex) {
  //     print(ex);
  //   }
  // }

  // static Database getDB() {
  //   return _db;
  // }

  // static void onCreate(Database db, int version) async {
  //   Batch batch = db.batch();

  //   // drop first
  //   batch.execute("DROP TABLE IF EXISTS todo_items ;");

  //   batch.execute("DROP TABLE IF EXISTS recurring_items ;");
  //   // then create again
  //   batch.execute(
  //       "CREATE TABLE todo_items (id INTEGER PRIMARY KEY NOT NULL, task STRING, complete BOOLEAN, taskdate num);");
  //   batch.execute(
  //       "CREATE TABLE recurring_items (id INTEGER PRIMARY KEY NOT NULL, task STRING, is_active BOOLEAN);");
  //   List<dynamic> result = await batch.commit();
  // }

  // static Future<List<Map<String, dynamic>>> query(String table) async =>
  //     _db.query(table);

  // static Future<int> insert(String table, Model model) async =>
  //     await _db.insert(table, model.toMap());

  // static Future<int> update(String table, Model model) async => await _db
  //     .update(table, model.toMap(), where: 'id = ?', whereArgs: [model.id]);

  // static Future<int> delete(String table, Model model) async =>
  //     await _db.delete(table, where: 'id = ?', whereArgs: [model.id]);
}
