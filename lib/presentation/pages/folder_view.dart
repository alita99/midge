import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:midge/data/models/play_list.dart';
import 'package:provider/provider.dart';
import 'package:audio_manager/audio_manager.dart';

class FolderView extends StatefulWidget {
  @override
  _FolderView createState() => _FolderView();
}

class _FolderView extends State<FolderView> {
  static const platform = const MethodChannel('midge.flutter.dev/music');
  List folders = new List();
  List<String> selectedFolders = new List<String>();
  bool isAdded = false;

  get cart => null;

  Future<void> getMp3FolderPaths() async {
    List externalFolders;
    try {
      final List folders = await platform.invokeMethod('getMp3FolderPaths');
      externalFolders = folders;
    } on PlatformException catch (e) {
      externalFolders = new List();
    }

    setState(() {
      folders = externalFolders;
    });
  }

  // Future<void> initPlayList(BuildContext context) async {
  //   if (folders.length > 0) {
  //     List mp3Files;
  //     try {
  //       var folderMap = folders[0] as Map;
  //       final List files = await platform.invokeMethod(
  //           'getMp3FilesFromFolderPath',
  //           {"folderPath": folderMap["file_path"]});
  //       mp3Files = files;
  //     } on PlatformException catch (e) {
  //       mp3Files = new List();
  //     }
  //     List<Map<String, String>> mp3FilesPath = addListData(mp3Files);
  //     Provider.of<PlayList>(context, listen: false).addAll(mp3FilesPath);
  //     isAdded = true;
  //   }
  // }

  @override
  Future<void> initState() {
    super.initState();
    getMp3FolderPaths();
  }

  Future<void> addPathsInPlayList(
      String folderPath, BuildContext context) async {
    List mp3Files;
    try {
      final List files = await platform.invokeMethod(
          'getMp3FilesFromFolderPath', {"folderPath": folderPath});
      mp3Files = files;
    } on PlatformException catch (e) {
      mp3Files = new List();
    }
    List<Map<String, String>> mp3FilesPath = addListData(mp3Files);
    Provider.of<PlayList>(context, listen: false).addAll(mp3FilesPath);
    addSongsInPlayer(mp3FilesPath);
  }

  void addSongsInPlayer(List<Map<String, String>> mp3FilesPath) {
    // List<Map<String, String>> listF =
    //     Provider.of<PlayList>(context, listen: false).getItems();
    List<AudioInfo> _list = [];
    mp3FilesPath.forEach((item) => _list.add(AudioInfo(item["url"],
        title: item["title"], desc: item["desc"], coverUrl: item["coverUrl"])));

    AudioManager.instance.audioList.addAll(_list);
    if (!AudioManager.instance.isPlaying) {
      AudioManager.instance.play(auto: true, index: 0);
    }
  }

  List<Map<String, String>> addListData(List mp3Files) {
    var listF = new List<Map<String, String>>();
    if (mp3Files != null && !mp3Files.isEmpty) {
      for (var file in mp3Files) {
        String path = file["file_path"];
        listF.add({
          "title": file["file_name"],
          "desc": "local file",
          "url": "file://${path}",
          "coverUrl": "https://homepages.cae.wisc.edu/~ece533/images/baboon.png"
        });
      }
    }
    return listF;
  }

  void addSelectedFolders(String folderPath) {
    Provider.of<PlayList>(context, listen: false)
        .addSelectedFolders(folderPath);
    // selectedFolders.add(folderPath);
    // print(selectedFolders);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: GridView.count(
      crossAxisCount: 4,
      children: List.generate(folders.length, (index) {
        var folder = folders[index] as Map;
        Color folderColour =
            (index % 2) == 0 ? Colors.blue[50] : Colors.pink[50];
        return InkWell(
          child: Tab(
              icon: Icon(
                (folder["selected"] == null ? false : folder["selected"])
                    ? Icons.folder_outlined
                    : Icons.folder_rounded,
                color: folderColour,
                size: 60,
              ),
              child: Text(folder["file_name"],
                  softWrap: true,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 13, color: Colors.white54))),
          onTap: () {
            addPathsInPlayList(folder["file_path"], context);
          },
          onLongPress: () {
            folder["selected"] = true;
            setState(() {
              folders[index] = folder;
            });
            addSelectedFolders(folder["file_path"]);
          },
        );
      }),
    ));
  }
}
