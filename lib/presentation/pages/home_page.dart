import 'package:flutter/material.dart';
import 'package:midge/data/models/play_list.dart';
import 'package:midge/main.dart';
import 'package:midge/presentation/pages/folder_view.dart';
import 'package:midge/presentation/player_bottom.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'music_list.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  bool _musicPlayer = false;
  Permission _permission;
  PermissionStatus _permissionStatus = PermissionStatus.undetermined;

  @override
  Future<void> initState() {
    super.initState();
    requestPermission(_permission);
  }

  void showMusicPlayer(int index) {
    print(index);
    if (index == 1) {
      setState(() {
        _musicPlayer = true;
      });
    } else {
      setState(() {
        _musicPlayer = false;
      });
    }
  }

  void hideMusicPlayer() {
    print("Clicked");
    setState(() {
      _musicPlayer = false;
    });
  }

  Future<void> requestPermission(Permission permission) async {
    permission = Permission.byValue(Permission.storage.value);
    final status = await permission.request();

    setState(() {
      print(status);
      _permissionStatus = status;
      print(_permissionStatus);
    });
  }

  playSelectedFoldersList() {
    List<String> folders =
        Provider.of<PlayList>(context, listen: false).getSelectedFolders();
  }

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      backgroundColor: new HexColor("#211f1f"),
      body: Container(
          child: Column(
        children: <Widget>[
          Visibility(
              visible: _musicPlayer == false,
              //child: Container(child: FolderView())),
              child: FolderView()),
          Visibility(
              visible: _musicPlayer == true,
              //child: Container(child: MusicList()),
              child: MusicList()),
          Align(alignment: Alignment.bottomCenter, child: PlayerBottom())
        ],
      )),
      floatingActionButton: Align(
          alignment: Alignment.topRight,
          child: FloatingActionButton(
            onPressed: () {
              playSelectedFoldersList();
            },
            autofocus: false,
            child: const Icon(Icons.thumb_up),
            backgroundColor: Colors.pink,
          )),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.music_note_rounded),
              activeIcon: Icon(Icons.music_note_rounded),
              label: "My music"),
          BottomNavigationBarItem(
              icon: Icon(Icons.playlist_play),
              activeIcon: Icon(Icons.playlist_play),
              label: "Playlist"),
        ],
        currentIndex: 0,
        selectedItemColor: new HexColor("#7a4aa1"),
        onTap: (int i) => showMusicPlayer(i),
        unselectedItemColor: Colors.white54,
        backgroundColor: new HexColor("#211f1f").withOpacity(1),
      ),
    );

    return SafeArea(top: true, bottom: true, child: scaffold);
  }
}
