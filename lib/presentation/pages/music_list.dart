import 'package:flutter/material.dart';

import 'package:audio_manager/audio_manager.dart';
import 'package:midge/data/models/play_list.dart';
import 'package:provider/provider.dart';

class MusicList extends StatefulWidget {
  @override
  _MusicList createState() => _MusicList();
}

class _MusicList extends State<MusicList> {
  var list = new List<Map<String, String>>();

  @override
  void initState() {
    super.initState();
  }

  getMp3FromProvider(BuildContext context) {
    List<Map<String, String>> listF =
        Provider.of<PlayList>(context, listen: false).getItems();
    setState(() {
      list = listF;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (list == null || list.isEmpty) {
      getMp3FromProvider(context);
    }

    return Expanded(
      child: ListView.separated(
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(list[index]["title"],
                  style: TextStyle(fontSize: 18, color: Colors.white54)),
              // subtitle: Text(list[index]["desc"],
              //     style: TextStyle(fontSize: 18, color: Colors.white54)),
              onTap: () => AudioManager.instance.play(index: index),
            );
          },
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemCount: list.length),
    );
  }
}
