import 'package:flutter/material.dart';
import 'package:midge/data/models/play_list.dart';
import 'package:midge/presentation/pages/home_page.dart';
import 'package:midge/presentation/pages/music_list.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    ChangeNotifierProvider(
      create: (context) => PlayList(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI',
      debugShowCheckedModeBanner: false,
      darkTheme: ThemeData.dark(),
      home: Home(),
      initialRoute: '/home',
      routes: {
        '/home': (BuildContext context) => Home(),
        '/musicList': (BuildContext context) => MusicList()
      },
    );
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
